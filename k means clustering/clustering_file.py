#importing of libraries needed for the k means clustering

import pandas as pd
import numpy as np
import random as rd
import matplotlib.pyplot as plt


data = pd.read_csv("clustering.csv")
print(data.head())

x = data[["LoanAmount", "ApplicantIncome"]]
# visualize the data points
print(x)

plt.scatter(x["ApplicantIncome"],x["LoanAmount"], c="black")
plt.xlabel("Annual Income")
plt.ylabel("LoanAmount (in thousands)")
plt.show()


# Step 1 and 2 - Choose the number of clusters (k) and select random centroid for each cluster

#number of clusters
k=3

# Select random obersavtion as centroids
centroids = (x.sample(n=k))
plt.scatter(x["ApplicantIncome"],x["LoanAmount"], c = "black")
plt.scatter(centroids["ApplicantIncome"], centroids["LoanAmount"], c = "red")
plt.xlabel("Annual Income")
plt.ylabel("Loan Amount (In Thousands)")
plt.show()


#Step 3 - Assign all the points to the closest cluster centroid
#Step 4 - Recompute centroids of newly formed clusters
#Step 5 - Repeat step 3 and 4

diff = 1
j = 0

while (diff!=0):
    xd = x
    i = 1
    for index1, row_c in centroids.iterrows():
#creating an empty list to input all the computed length of all points
        ed =[]
#calculating the euclidean distance to calculate the distance all points have to the current centroids
        for index2,row_d in xd.iterrows():
            d1=(row_c["ApplicantIncome"]-row_d["ApplicantIncome"])**2
            d2=(row_c["LoanAmount"]-row_d["LoanAmount"])**2
            d=np.sqrt(d1+d2)
#append the distance of that given and calculated pint to the list
            ed.append(d)
#go into the next entry inside the list to not override the former result
        x[i]=ed
        i = i+1
#create a new empty list for ???
    c = []
    for index,row in x.iterrows():
        print(row[1])
        min_dist=row[1]
        pos=1
        for i in range (k):
            if row[i+1] < min_dist:
                min_dist = row[i+1]
                pos= i+1
        c.append(pos)
    x["Cluster"]=c
    centroids_new = x.groupby(["Cluster"]).mean()[["LoanAmount","ApplicantIncome"]]
    if j == 0:
        diff=1
        j=j+1
    else:
        diff = (centroids_new["LoanAmount"] - centroids["LoanAmount"]).sum() + (centroids_new["ApplicantIncome"] - centroids["ApplicantIncome"]).sum()
        print(diff.sum())
    centroids = x.groupby(["Cluster"]).mean()[["LoanAmount","ApplicantIncome"]]

color=["blue","green","cyan"]
for o in range(k):
    data=x[x["Cluster"]==o+1]
    plt.scatter(data["ApplicantIncome"],data["LoanAmount"],c=color[o])
plt.scatter(centroids["ApplicantIncome"],centroids["LoanAmount"], c="red")
plt.xlabel("Income")
plt.ylabel("Loan Amount (in thousands) ")
plt.show()